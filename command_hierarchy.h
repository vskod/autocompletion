#ifndef COMMAND_HIERARCHY_H
#define COMMAND_HIERARCHY_H

#include <map>
#include <string>

class Layer
{
public:
    explicit Layer(std::string n, Layer* p = nullptr): name_(n), parent_(p) {}
    virtual ~Layer() = default;
    std::string name() { return name_; }
    Layer* parent() { return parent_; }
private:
    std::string name_;
    Layer* parent_;
};

class Base : public Layer { using Layer::Layer; };
class Commands: public Layer { using Layer::Layer; };
class Options: public Layer { using Layer::Layer; };

typedef std::multimap<Layer*, Layer*> map;

void print_hierarchy(Base &base, const map &m);
void command_hierarchy(char *argv[], Base &base, const map &m);

#endif // COMMAND_HIERARCHY_H
