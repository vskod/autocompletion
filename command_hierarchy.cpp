#include "command_hierarchy.h"
#include <iostream>

void command_hierarchy(char *argv[], Base &base, const map &m)
{
    auto commands = m.equal_range(&base);
    for (auto it = commands.first; it != commands.second; ++it)
    {
         auto commands = it->second;
         if (argv[1] == commands->name())
         {
             auto options = m.equal_range(commands);
             for (auto it = options.first; it != options.second; ++it)
             {
                  auto options = it->second;
                  if (argv[2] != 0 && argv[2] == options->name())
                  {
                        std::cout << "choised command: " << commands->name() << " " << options->name() << std::endl;
                        return;
                  }
             }
         }
         else if (argv[1] == std::string("help"))
         {
             std::cout << "help: " << std::endl;
             print_hierarchy(base, m);
             return;
         }
    }
    std::cout << "Use <help> to see correct commands and options: " << std::endl;
}
