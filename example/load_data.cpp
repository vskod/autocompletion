#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main()
{
    vector<string> all_lines;
    fstream newfile;

    newfile.open("strings.txt");

    if (newfile.is_open())
    {
       string data;
       while(getline(newfile, data))
       {
          cout << data << endl;
          all_lines.push_back(data);
       }

       if (!all_lines.size())
       {
           cout << "data file is empty" << endl;
           return 1;
       }

       cout << "total data_vector size " << all_lines.size() << endl;
       newfile.close();
    }
}
