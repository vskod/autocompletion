#include "command_hierarchy.h"
#include <map>
#include <string>
#include <utility>
#include <vector>
#include <iostream>

void print_hierarchy(Base &base, const map &m)
{
    auto commands = m.equal_range(&base);

    if (commands.first == commands.second)
    {
        std::cout << "Commands not found" << std::endl;
        return;
    }

    std::cout << "Commands on " << commands.first->first->name() << " :\n";
    for (auto it = commands.first; it != commands.second; ++it)
    {
         auto commands = it->second;
         std::cout << commands->name() << " (located in " << commands->parent()->name() << "), with options:\n";
         auto options = m.equal_range(commands);
         for (auto it = options.first; it != options.second; ++it)
         {
              auto option = it->second;
              std::cout << "     " << option->name() << " (located in options " << option->parent()->name() << ")\n";
         }
         std::cout << "\n";
    }
}

int main(int argc, char *argv[])
{
    if (argc <= 1)
    {
        if (argv[0])
            std::cout << "Usage: " << argv[0] << " <--command>" << std::endl;
        return 1;
    }

    Base base("Base level");

    Commands light("light", &base);
    Commands door("door", &base);
    Commands window("window", &base);

    std::vector<Options> light_options = { Options("on", &light), Options("off", &light) };
    std::vector<Options> door_options  = { Options("open", &door), Options("close", &door) };
    std::vector<Options> window_options = { Options("open", &window), Options("half-open", &window), Options("close", &window) };

    map m;

    m.insert({&base, &light});
    m.insert({&base, &door});
    m.insert({&base, &window});

    for (auto& options: light_options)
        m.insert({&light, &options});

    for (auto& options: door_options)
        m.insert({&door, &options});

    for (auto& options: window_options)
        m.insert({&window, &options});

    command_hierarchy(argv, base, m);

    return 0;
}
