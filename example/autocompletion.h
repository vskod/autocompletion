#include <iostream>

class Commands
{
public:

    std::string commands[4] = {"light", "door", "window", "jalousie"};

    void execute()
    {
    }

    std::string commandName(int index)
    {
        return commands[index];
    }

};

void readParameters(int argc, char *argv[])
{
    std::cout << "argc = " << argc << std::endl;
    for(int pos = 1; pos < argc; pos++)
    {
        std::cout << "argv[" << pos << "] = " << argv[pos] << std::endl;
    }
}
