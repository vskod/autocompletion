#include <iostream>
#include <string>
#include "autocompletion.h"

int main(int argc, char *argv[])
{
    if (argc <= 1)
    {
        if (argv[0])
            std::cout << "Usage: " << argv[0] << " <--option>" << std::endl;
        exit(1);
    }

    if (argv[1] == std::string("--help"))
    {
        std::cout << "help TBD: " << std::endl;
    }
    else
    {
        readParameters(argc, argv);
    }

    return 0;
}


